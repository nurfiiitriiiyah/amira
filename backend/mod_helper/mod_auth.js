const jwt = require("jsonwebtoken");
const config = require("../../config/main_config");

function authCheck(token) {
    return new Promise((resolve, reject) => {
        if (!token) {
            reject({
                message: 'No token provided.'
            });
        }
        else {
            jwt.verify(token, config.jwt_conf.secret, function (err, decoded) {
               if (err) {
                    reject({
                        message: 'Failed to authenticate token.'
                    });
                }
                else {
                    resolve({
                        status: "ok",
                        data: decoded
                    })
                }
            });
        }
    });
}

module.exports = {
    authCheck
};