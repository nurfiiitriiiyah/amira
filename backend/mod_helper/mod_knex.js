const config = require('../../config/main_config');
const mysql = require('mysql');

const knex = require('knex')({
    client: 'mysql',
    connection: {
        host     : config.prod.db_bost,
        user     : config.prod.db_uname,
        password : config.prod.db_pass,
        database : config.prod.db_name
    },
    pool: { min: 0, max: 10 }
});

module.exports = knex;
