// var jwt = require("jsonwebtoken");
// const bcrypt = require('bcrypt-nodejs');
// const db = require("../../mod_helper/mod_db");
// const conf = require("../../config/main_config");
//
// function login(data) {
//     return new Promise((resolve, reject) => {
//         db.query("SELECT *FROM tb_user WHERE user_uname = ?", [data.sendUname], function (err, res, rows) {
//             if (err) {
//                 reject(err);
//                 throw err;
//             }
//             else {
//                 if (res.length > 0) {
//                     if(data.sendPass === res[0].user_password){
//                         console.log("==================1=================");
//                         let data = {
//                             id: res[0].user_id,
//                             name: res[0].user_name,
//                             phone: res[0].user_phone,
//                             email: res[0].user_email,
//                             position: res[0].user_position,
//                             role: res[0].user_role
//                         };
//                         resolve({
//                             status: "ok",
//                             data: {
//                                 message: "Login Berhasil",
//                                 token: jwt.sign(data, conf.jwt_conf.secret)
//                             }
//                         })
//                     }
//                     else{
//                         console.log("==================0=================");
//                         resolve({
//                             status: "nok",
//                             data: "Password Salah"
//                         });
//                     }
//                 }
//                 else {
//                     console.log("==================0=================");
//                     resolve({
//                         status: "nok",
//                         data: "Username Salah"
//                     });
//                 }
//             }
//         });
//     });
// }
//
//
// module.exports = {
//     login
// };
/**
 * Modul Untuk Login
 * @author Muhammad Reyhan Abizar
 * @version 1.0
 * @since 02/11/2018
 */


/**
 * Module dependencies.
 */

const jwt = require("jsonwebtoken");
const knex = require("../mod_helper/mod_knex");
const conf = require("../../config/main_config");
const bcrypt = require('bcrypt-nodejs');

/**
 * Fungsi Untuk Autentikasi User Login
 * @async
 * @function
 * @param {object} data      JSON yang data username & password
 * @return {Promise}         status dan message sukses
 */

function login(data) {
return new Promise((resolve, reject) => {

    try{
        knex(`tb_user`)
            .select('*')
            .where(`user_uname`, data.sendUname)
            .then((result) => {
            if(result.length > 0){
                if(bcrypt.compareSync(data.sendPass, result[0].user_password)){
                    let data = {
                        id: result[0].user_id,
                        nama: result[0].user_name,
                        role: result[0].user_role
                    };
                    resolve({
                        status: "ok",
                        data: {
                            message: "Login Berhasil",
                            token: jwt.sign(data, conf.jwt_conf.secret)
                        }
                    })
                } else {
                    resolve({
                        status: "nok",
                        data: "Password Salah"
                    });
                }
            } else {
                resolve({
                    status: "nok",
                    data: "Username Tidak Terdaftar"
                });
            }
        }).catch((error) => {
            reject(error.message);
            throw error;
        });
    }
    catch(error){
        reject(error.message);
    }
});

}


module.exports = {
    login
};
