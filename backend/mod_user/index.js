/**
 * Modul Untuk Manajemen Product
 * @author Nur Fitriyah
 * @version 1.0
 * @since 12/11/2018
 */


/**
 * Module dependencies.
 */

const bcrypt = require(`bcrypt-nodejs`);
const validator = require(`validator`);
const moment = require('moment');
const knex = require("../mod_helper/mod_knex");

/**
 * Module dependencies combo box role.
 */

function validateInputUser(data) {
    return new Promise((resolve, reject) => {
        let valid = true;
        let message = [];
        if (validator.isEmpty(data.userName)) {
            valid = false;
            message.push("Nama tidak boleh kosong");
        }
        if (validator.isEmpty(data.userTelephone)) {
            valid = false;
            message.push("Telepon tidak boleh kosong");
        }
        if (validator.isEmpty(data.userEmail)) {
            valid = false;
            message.push("Email tidak boleh kosong");
        }
        if (validator.isEmpty(data.userRole)) {
            valid = false;
            message.push("Role tidak boleh kosong");
        }
        if (validator.isEmpty(data.userPass)) {
            valid = false;
            message.push("Password tidak boleh kosong");
        }
        checkDuplicateUsername(data.userUname).then((response) => {
            if (response) {
                valid = false;
                message.push("Username telah terdaftar");
            }

            if (valid) {
                resolve({
                    data: "Validasi Sukses"
                });
            }
            else {
                reject({
                    message: message
                });
            }
        }).catch((error) => {
            reject({
                message: error
            });
        });
    });
}

/**
 * Fungsi Untuk Cek Username Yang Telah Terdaftar di Database
 *
 * @param {string} username      String username yang ingin di check
 * @return {Promise}             boolean dari pengecekan username
 */
function checkDuplicateUsername(username) {
    return new Promise((resolve, reject) => {
        knex(`tb_user`).count(`user_id as jumlah`).where(`user_uname`, username).then((result) => {
            resolve(result[0].jumlah > 0);
        }).catch((error) => {
            reject(error.message);
        });
    });
}

function InsertUser(data, decoded) {
    return new Promise((resolve, reject) => {
        validateInputUser(data).then((respValidate) => {
            let dataInsert = {
                user_name: data.userName,
                user_phone: data.userTelephone,
                user_email: data.userEmail,
                user_address: data.userAddress,
                user_position: "1",
                user_uname: data.userUname,
                user_password: bcrypt.hashSync(data.userPass),
                user_role: data.userRole,
                user_status: "1",
                created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                created_by: decoded.data.id,
                updated_by: decoded.data.id
            };
            knex.insert(dataInsert).into("tb_user").then((result) => {
                resolve({
                    status: "ok",
                    data: "User Berhasil Ditambahkan"
                });
            }).catch((error) => {
                reject(error.message);
            });
        }).catch((error) => {
            resolve({
                status: "nok",
                data: error.message
            });
        });
    });
}

function getUser(start, length, draw, order, custom_search, decoded) {
    console.log("****MOD****");

    return new Promise((resolve, reject) => {
        let orderDir = order[0].dir;
        let orderColumn = parseInt(order[0].column);
        let query = knex(`tb_user as user`)
            .select(`*`)
            .join(`tb_user_role as role`, `user.user_role`, `=`, `role.user_role_id`)
            .where(`user.user_name`, `like`, `%${custom_search.name}%`)
            .where(`user.user_phone`, `like`, `%${custom_search.telephone}%`)
            .where(`user.user_email`, `like`, `%${custom_search.email}%`)
            .where(`user.user_address`, `like`, `%${custom_search.address}%`)
            .where(`user.user_role`, `like`, `%${custom_search.roles}%`)
            .where(`user.user_uname`, `like`, `%${custom_search.uname}%`)
        ;

        switch (orderColumn) {
            case 0:
                query = query.orderBy('user.user_id', orderDir);
                break;
            case 1:
                query = query.orderBy('user.user_name', orderDir);
                break;
            case 2:
                query = query.orderBy('user.user_phone', orderDir);
                break;
            case 3:
                query = query.orderBy('role.user_email', orderDir);
                break;
            case 4:
                query = query.orderBy('user.user_address', orderDir);
                break;
            case 5:
                query = query.orderBy('user.user_uname', orderDir);
                break;
            case 6:
                query = query.orderBy('user.user_status', orderDir);
                break;
            case 7:
                query = query.orderBy('user.user_role', orderDir);
                break;
            default:
                query = query.orderBy('user.user_id', 'DESC');
                break;
        }
        query.limit(length).offset(start).then((result) => {
            query.then((resultFull) => {

                let data = [];
                for (let i = 0; i < result.length; i++) {
                    data[i] = [];
                    data[i][0] = i + 1;
                    data[i][1] = result[i].user_name;
                    data[i][2] = result[i].user_phone;
                    data[i][3] = result[i].user_email;
                    data[i][4] = result[i].user_address;
                    data[i][5] = result[i].user_uname;
                    data[i][6] = result[i].user_status;
                    data[i][7] = result[i].user_role_label;
                    data[i][8] = result[i].user_id;
                }
                resolve({
                    data: data,
                    recordsTotal: resultFull.length,
                    recordsFiltered: resultFull.length
                });
            })
        }).catch((error) => {
            reject(error.message);
        });
    });
}

/**
 * Fungsi Untuk Detail Product
 *
 * @param {string} Detail Product
 * @return {Promise} boolean dari Detail Product
 */
function getDetailUser(id, decoded) {
    return new Promise((resolve, reject) => {
        knex(`tb_user as user`)
            .join(`tb_user_role as role`, `user.user_role`, `=`, `role.user_role_id`)
            .select(`user_status`,`user.user_id`,`user.user_uname`,`user.user_address`,`user.user_name`, `user.user_phone`, `user.user_email`, `role.user_role_label`, `user.user_address`)
            .where('user.user_id', id)
            .then((result) => {
                resolve({
                    status: "ok",
                    data: result[0]
                });
            })
            .catch((error) => {
                    reject(error.message);
                }
            );
    });
}

function validateEditUser(data) {
    console.log(data);
    return new Promise((resolve, reject) => {
        let valid = true;
        let message = [];
        if (validator.isEmpty(data.userEditPhone)) {
            valid = false;
            message.push("Nama tidak boleh kosong");
        }
        if (validator.isEmpty(data.userEditEmail)) {
            valid = false;
            message.push("Telepon tidak boleh kosong");
        }
        if (validator.isEmpty(data.userEditAddress)) {
            valid = false;
            message.push("Email tidak boleh kosong");
        }
        if (validator.isEmpty(data.userEditName)) {
            valid = false;
            message.push("Role tidak boleh kosong");
        }
        if (valid) {
            resolve({
                data: "Validasi Sukses"
            });
        }
        else {
            reject({
                message: message
            });
        }
    });
}

function EditUser(data, decoded) {
    return new Promise((resolve, reject) => {
        validateEditUser(data).then((respValidate) => {
            let dataUpdate = {
                user_phone: data.userEditPhone,
                user_email: data.userEditEmail,
                user_address: data.userEditAddress,
                user_name: data.userEditName,
                updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                updated_by: decoded.data.id
            };
            knex('tb_user')
                .where('user_id', data.userEditID)
                .update(dataUpdate)
                .then((result) => {
                    resolve({
                        status: "ok",
                        data: "Data berhasil diubah"
                    });
                }).catch((error) => {
                    reject(error.message);
                }
            );
        });
    });
}

function activateUser(data, decoded) {
    return new Promise((resolve, reject) => {
            let dataUpdate = {
                user_status: "1",
                updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                updated_by: decoded.data.id
            };
            knex('tb_user')
                .where('user_id', data.userActivateID)
                .update(dataUpdate)
                .then((result) => {
                    resolve({
                        status: "ok",
                        data: "User Berhasil Diaktifkan"
                    });
                }).catch((error) => {
                    reject(error.message);
                }
            );
        });
}

function deactivateUser(data, decoded) {
    return new Promise((resolve, reject) => {
            let dataUpdate = {
                user_status: "0",
                updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                updated_by: decoded.data.id
            };
            knex('tb_user')
                .where('user_id', data.userDectivateUser)
                .update(dataUpdate)
                .then((result) => {
                    resolve({
                        status: "ok",
                        data: "User Berhasil Dinonaktifkan"
                    });
                }).catch((error) => {
                    reject(error.message);
                }
            );
        });
}
module.exports = {
    activateUser,
    deactivateUser,
    InsertUser,
    EditUser,
    getDetailUser,
    getUser
};