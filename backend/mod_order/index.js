/**
 * Modul Untuk Manajemen User
 * @author Muhammad Reyhan Abizar
 * @version 1.0
 * @since 02/11/2018
 */

/**
 * Module dependencies.
 */

const bcrypt = require(`bcrypt-nodejs`);
const knex = require("../mod_helper/mod_knex");
const validator = require(`validator`);
const fs = require(`fs`);
const moment = require('moment');


function getProduct(decoded) {
    return new Promise((resolve, reject) => {
        knex.select('*').from('tb_product').where('product_status', "1").then((result) => {
            resolve({
                status: "ok",
                data: result
            });
        }).catch((error) => {
            reject(error.message);
        });
    });
}

function getOrderSelect(data, decoded) {
    var orderDate = data.orderProdictDate;
    var orderProductType = data.orderProductType;
    return new Promise((resolve, reject) => {
        knex(`tb_order`)
            .select(`order_finish_date`)
            .where('product_id', orderProductType)
            .then((result) => {
                if (result.length < 1) {
                    resolve({
                        status: "ok",
                        data: result
                    });
                }
                else {
                    var finishDate = result[0].order_finish_date;
                    console.log(finishDate);
                    console.log("______________");
                    console.log(orderDate);
                    if (finishDate >= orderDate) {
                        console.log("gak bisa");
                    }
                    else {
                        console.log("bisa");
                    }
                }

                console.log();

            })
            .catch((error) => {
                    reject(error.message);
                }
            );
    });
}

function validateInsertOrder(data) {
    return new Promise((resolve, reject) => {
        let valid = true;
        let message = [];
        if (validator.isEmpty(data.productCostumer)) {
            valid = false;
            message.push("Nama Pelanggan tidak boleh kosong");
        }
        if (validator.isEmpty(data.productID)) {
            valid = false;
            message.push("Produk tidak boleh kosong");
        }
        if ((validator.isEmpty(data.productPrice)) || data.productPrice === "0") {
            valid = false;
            message.push("Harga Produk tidak boleh kosong");
        }
        if ((validator.isEmpty(data.productPayment)) || data.productPayment === "0") {
            valid = false;
            message.push("Uang Muka Tidak boleh kosong");
        }
        if (validator.isEmpty(data.productAddress)) {
            valid = false;
            message.push("Password tidak boleh kosong");
        }
        if (validator.isEmpty(data.productDestination)) {
            valid = false;
            message.push("Password tidak boleh kosong");
        }
        if (valid) {
            resolve({
                data: "Validasi Sukses"
            });
        }
        else {
            reject({
                message: message
            });
        }
    });
}

function InsertOrder(data, decoded) {
    return new Promise((resolve, reject) => {
        validateInsertOrder(data).then((respValidate) => {
            var orderID = "INV" + moment().format("YYYYMMDDHHmmss");
            var startDateSub = data.productDateRange.substr(0, 10);
            var finishDateSub = data.productDateRange.substr(13, 22);
            var startDate = new Date(startDateSub);
            var finishDate = new Date(finishDateSub);
            var statusPembayaran;
            if(parseInt(data.productPaymentRemain) >= 1){
                statusPembayaran = "2"
            }
            else{
                statusPembayaran = "1"
            }
            let dataInsert = {
                order_id: orderID,
                product_id: data.productID,
                order_customer: data.productCostumer,
                order_start_date: startDate,
                order_finish_date: finishDate,
                order_price: data.productPrice,
                order_customer_phone: data.productTelephone,
                order_down_payment: data.productPayment,
                order_remaining_payment: data.productPaymentRemain,
                order_pick_up: data.productAddress,
                order_destination: data.productDestination,
                order_status: statusPembayaran,
                created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                created_by: decoded.data.id,
                updated_by: decoded.data.id
            };
            knex.insert(dataInsert).into("tb_order").then((result) => {
                resolve({
                    status: "ok",
                    data: "User Berhasil Ditambahkan"
                });
            }).catch((error) => {
                reject(error.message);
            });
        }).catch((error) => {
            resolve({
                status: "nok",
                data: error.message
            });
        });
    });
}


function getOrder(start, length, draw, order, custom_search, decoded) {

    return new Promise((resolve, reject) => {
        let orderDir = order[0].dir;
        let orderColumn = parseInt(order[0].column);
        console.log(custom_search);
        let query = knex(`tb_order as orders`)
            .select(`orders.*`, `product_label`)
            .join(`tb_product as products`, `products.product_id`, `orders.product_id`)
            .where(`orders.order_customer`, `like`, `%${custom_search.name}%`)
            .where(`orders.order_customer_phone`, `like`, `%${custom_search.telephone}%`)
            // .whereBetween(`orders.date(created_at)`, [custom_search.startDate, custom_search.endDate])
            .where  (knex.raw('DATE_FORMAT(orders.created_at,"%Y-%m-%d") between ' +'"'+custom_search.startDate + '" and "' + custom_search.endDate + '"'))
            .where(`orders.product_id`, `like`, `%${custom_search.product}%`)
            .where(`orders.order_status`, `like`, `%${custom_search.status}%`)
        ;


        switch (orderColumn) {
            case 0:
                query = query.orderBy('order_id', orderDir);
                break;
            case 1:
                query = query.orderBy('order_customer', orderDir);
                break;
            case 2:
                query = query.orderBy('order_customer_phonee', orderDir);
                break;
            case 3:
                query = query.orderBy('product_id', orderDir);
                break;
            case 4:
                query = query.orderBy('order_start_date', orderDir);
                break;
            case 5:
                query = query.orderBy('order_price', orderDir);
                break;
            case 6:
                query = query.orderBy('order_down_payment', orderDir);
                break;
            case 7:
                query = query.orderBy('order_remaining_payment', orderDir);
                break;
            case 8:
                query = query.orderBy('order_pick_up', orderDir);
                break;
            case 9:
                query = query.orderBy('order_destination', orderDir);
                break;
            case 10:
                query = query.orderBy('order_status', orderDir);
                break;
            default:
                query = query.orderBy('order_id', 'DESC');
                break;
        }
        query.limit(length).offset(start).then((result) => {
            query.then((resultFull) => {

                let data = [];
                for (let i = 0; i < result.length; i++) {
                    data[i] = [];
                    data[i][0] = i + 1;
                    data[i][1] = result[i].order_customer;
                    data[i][2] = result[i].order_customer_phone;
                    data[i][3] = result[i].product_label;
                    data[i][4] = result[i].order_start_date;
                    data[i][5] = result[i].order_price;
                    data[i][6] = result[i].order_down_payment;
                    data[i][7] = result[i].order_remaining_payment;
                    data[i][8] = result[i].order_pick_up;
                    data[i][9] = result[i].order_destination;
                    data[i][10] = result[i].order_status;
                    data[i][11] = result[i].order_id;
                }
                resolve({
                    data: data,
                    recordsTotal: resultFull.length,
                    recordsFiltered: resultFull.length
                });
            })
        }).catch((error) => {
            reject(error.message);
        });
    });
}

function getDetailOrder(id, decoded) {
    return new Promise((resolve, reject) => {
        knex(`tb_order as orders`)
            .join(`tb_product as product`, `product.product_id`, `=`, `orders.product_id`)
            .select(`orders.*`, `product_label`, `product_image`, `product_spesification`)
            .where('orders.order_id', id)
            .then((result) => {
                resolve({
                    status: "ok",
                    data: result[0]
                });
            })
            .catch((error) => {
                    reject(error.message);
                }
            );
    });
}

function rejectOrder(data, decoded) {
    return new Promise((resolve, reject) => {
        try {
            let dataUpdate = {
                order_notes: data.notes,
                order_status: 3,
                updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                updated_by: decoded.data.id
            };


            knex('tb_order')
                .where('order_id', data.order_id)
                .update(dataUpdate)
                .then((result) => {
                    resolve({
                        status: "ok",
                        data: "Inventory Berhasil Direject"
                    });
                }).catch((error) => {
                    reject(error.message);
                }
            );
        }
        catch (e) {
            console.log(e);
        }

    });
}

function PaymentDone(data, decoded) {
    return new Promise((resolve, reject) => {
        try {
            knex(`tb_order as orders`)
                .select(`*`)
                .where('orders.order_id', data.order_id)
                .then((result) => {
                    var paymentRemain = result[0].order_price;
                    let dataUpdate = {
                        order_remaining_payment: "0",
                        order_down_payment: paymentRemain,
                        order_notes: data.notes,
                        order_status: 1,
                        updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                        updated_by: decoded.data.id
                    };
                    knex('tb_order')
                        .where('order_id', data.order_id)
                        .update(dataUpdate)
                        .then((result) => {
                            resolve({
                                status: "ok",
                                data: "Pembayaran Berhasil DIlunasi"
                            });
                        }).catch((error) => {
                            reject(error.message);
                        }
                    );
                })
                .catch((error) => {
                        reject(error.message);
                    }
                );
        }
        catch (e) {
            console.log(e);
        }

    });
}


module.exports = {
    getProduct,
    getOrderSelect,
    InsertOrder,
    getOrder,
    getDetailOrder,
    rejectOrder,
    PaymentDone
};
