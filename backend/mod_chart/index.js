/**
 * Modul Untuk Manajemen Menu
 * @author Muhammad Reyhan Abizar
 * @version 1.0
 * @since 02/11/2018
 */


/**
 * Module dependencies.
 */

const knex = require("../mod_helper/mod_knex");

function getChart(decoded) {
    return new Promise((resolve, reject) => {
        knex(`tb_order`)
            .select(knex.raw(`order_customer,order_start_date,DATE_FORMAT(order_start_date,'%M') as bulan,count(*) as total `))
            .where('order_status', "1")
            .groupBy(knex.raw(`substr(order_start_date,1,7)`))
            .then((result) => {
                var i = 0;
                var resultM = "";
                for (i; i < result.length; i++) {
                    resultM = resultM + `"${result[i].bulan}"` + ",";
                }
                // resultM = JSON.parse("[" + resultM + "]");
                resolve({
                    status: "ok",
                    data: {
                        data: result,
                    }
                });
            }).catch((error) => {
            reject(error.message);
        });
    });
}

module.exports = {
    getChart
};