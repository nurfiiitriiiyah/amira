/**
 * Modul Untuk Manajemen Product
 * @author Nur Fitriyah
 * @version 1.0
 * @since 12/11/2018
 */


/**
 * Module dependencies.
 */

const validator = require(`validator`);
const moment = require('moment');
const knex = require("../mod_helper/mod_knex");
const fs = require(`fs`);

/**
 * Module dependencies combo box role.
 */

function validateInputProduct(data) {
    return new Promise((resolve, reject) => {
        let valid = true;
        let message = [];
        if (validator.isEmpty(data.productName)) {
            valid = false;
            message.push("Nama tidak boleh kosong");
        }
        if (validator.isEmpty(data.productSpesification)) {
            valid = false;
            message.push("Password tidak boleh kosong");
        }
        if (valid) {
            resolve({
                data: "Validasi Sukses"
            });
        }
        else {
            reject({
                message: message
            });
        }
    });
}

function InsertProduct(data, decoded) {
    // console.log(data);
    return new Promise((resolve, reject) => {
        validateInputProduct(data).then((respValidate) => {
            uploadFoto(data.productPhoto).then((respUpload) => {
                let dataInsert = {
                    product_label: data.productName,
                    product_spesification: data.productSpesification,
                    product_status: "1",
                    product_image: respUpload.path,
                    created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                    updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                    created_by: decoded.data.id,
                    updated_by: decoded.data.id
                };
                knex.insert(dataInsert).into("tb_product").then((result) => {
                    resolve({
                        status: "ok",
                        data: "Produk Berhasil Ditambahkan"
                    });
                }).catch((error) => {
                    reject(error.message);
                });
            }).catch((error) => {
                reject(error.message);
            });
        }).catch((error) => {
            resolve({
                status: "nok",
                data: error.message
            });
        });
    });
}

/**
 * Fungsi Untuk Upload File
 *
 * @param {object} file      Object FILE dari Formidable (https://github.com/utatti/express-formidable)
 * @return {Promise}         status dan path upload foto
 */

function uploadFoto(file) {
    return new Promise((resolve, reject) => {
        let extension;
        if (file.type === "image/jpeg") {
            extension = ".jpeg";
        } else if (file.type === "image/jpg") {
            extension = ".jpg";
        } else if (file.type === "image/png") {
            extension = ".png";
        }


        if(extension !== undefined){
            let oldPath = file.path;
            let newPath = "uploads/" + new Date().getTime() + extension;

            fs.readFile(oldPath, function (err, data) {
                if (err) {
                    reject(err.message);
                }
                fs.writeFile(newPath, data, function (err) {
                    if (err) {
                        reject(err.message)
                    }
                });
                fs.unlink(oldPath, function (err) {
                    if (err) {
                        reject(err.message);
                    }
                });

                resolve({
                    data: "Upload Sukses",
                    path: newPath
                });
            });
        }
        else{
            resolve({
                data: "No File",
                path: ""
            });
        }
    });
}
function getProduct(start, length, draw, order, custom_search, decoded) {
   console.log(custom_search);
    return new Promise((resolve, reject) => {
        let orderDir = order[0].dir;
        let orderColumn = parseInt(order[0].column);
        let query = knex(`tb_product as product`)
            .select(`*`)
            .join(`tb_user as user_created_by`, `product.created_by`, `=`, `user_created_by.user_id`)
            .join(`tb_user as user_updated_by`, `product.created_by`, `=`, `user_updated_by.user_id`)
            .where(`product.product_label`, `like`, `%${custom_search.name}%`)
            .where(`product.product_status`, `like`, `%${custom_search.status}%`)
        ;
        switch (orderColumn) {
            case 0:
                query = query.orderBy('product.product_id', orderDir);
                break;
            case 1:
                query = query.orderBy('product.product_label', orderDir);
                break;
            case 2:
                query = query.orderBy('product.product_status', orderDir);
                break;
            default:
                query = query.orderBy('product.product_id', 'DESC');
                break;
        }
        query.limit(length).offset(start).then((result) => {
            query.then((resultFull) => {

                let data = [];
                for (let i = 0; i < result.length; i++) {
                    data[i] = [];
                    data[i][0] = i + 1;
                    data[i][1] = result[i].product_label;
                    data[i][2] = result[i].product_status;
                    data[i][3] = result[i].product_id;
                }
                resolve({
                    data: data,
                    recordsTotal: resultFull.length,
                    recordsFiltered: resultFull.length
                });
            })
        }).catch((error) => {
            reject(error.message);
        });
    });
}

/**
 * Fungsi Untuk Detail Product
 *
 * @param {string} Detail Product
 * @return {Promise} boolean dari Detail Product
 */
function getDetailProduct(id, decoded) {
    return new Promise((resolve, reject) => {
        knex(`tb_product as product`)
            .select(`product.*`,`user_created_by.user_name as created_by`,`user_updated_by.user_name as updated_by`)
            .join(`tb_user as user_created_by`, `product.created_by`, `=`, `user_created_by.user_id`)
            .join(`tb_user as user_updated_by`, `product.created_by`, `=`, `user_updated_by.user_id`)
            .where('product.product_id', id)
            .then((result) => {
                resolve({
                    status: "ok",
                    data: result[0]
                });
            })
            .catch((error) => {
                    reject(error.message);
                }
            );
    });
}

function validateEditProduct(data) {
    console.log(data);
    return new Promise((resolve, reject) => {
        let valid = true;
        let message = [];
        if (validator.isEmpty(data.prouctEditSpefication)) {
            valid = false;
            message.push("Spesifikasi Produk tidak boleh kosong");
        }
        if (validator.isEmpty(data.productEditName)) {
            valid = false;
            message.push("Nama Produk tidak boleh kosong");
        }
        if (valid) {
            resolve({
                data: "Validasi Sukses"
            });
        }
        else {
            reject({
                message: message
            });
        }
    });
}

function EditProduct(data, decoded) {
    return new Promise((resolve, reject) => {
        validateEditProduct(data).then((respValidate) => {
            let dataUpdate = {
                product_label: data.productEditName,
                product_spesification: data.prouctEditSpefication,
                updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                updated_by: decoded.data.id
            };
            knex('tb_product')
                .where('product_id', data.productEditID)
                .update(dataUpdate)
                .then((result) => {
                    resolve({
                        status: "ok",
                        data: "Data berhasil diubah"
                    });
                }).catch((error) => {
                    reject(error.message);
                }
            );
        });
    });
}

function activateProduct(data, decoded) {
    return new Promise((resolve, reject) => {
        let dataUpdate = {
            product_status: "1",
            updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
            updated_by: decoded.data.id
        };
        knex('tb_product')
            .where('product_id', data.productActivateID)
            .update(dataUpdate)
            .then((result) => {
                resolve({
                    status: "ok",
                    data: "Produk Berhasil Diaktifkan"
                });
            }).catch((error) => {
                reject(error.message);
            }
        );
    });
}

function deactivateProduct(data, decoded) {
    return new Promise((resolve, reject) => {
        let dataUpdate = {
            product_status: "0",
            updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
            updated_by: decoded.data.id
        };
        knex('tb_product')
            .where('product_id', data.userDectivateUser)
            .update(dataUpdate)
            .then((result) => {
                resolve({
                    status: "ok",
                    data: "Produk Berhasil Diaktifkan"
                });
            }).catch((error) => {
                reject(error.message);
            }
        );
    });
}
module.exports = {
    activateProduct,
    deactivateProduct,
    InsertProduct,
    EditProduct,
    getDetailProduct,
    getProduct
};