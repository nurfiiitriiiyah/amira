/**
 * Modul Untuk Manajemen Menu
 * @author Muhammad Reyhan Abizar
 * @version 1.0
 * @since 02/11/2018
 */


/**
 * Module dependencies.
 */

const knex = require("../mod_helper/mod_knex");

function getMenu(decoded) {
    return new Promise((resolve, reject) => {
       knex(`tb_menu_matrix as matrix`)
            .select(`*`)
            .join(`tb_menu as menu`, `menu.menu_id`, `=`, `matrix.menu_id`)
            .where('role_id', decoded.data.role)
            .where('menu_matrix_status', 1)
            .orderBy('menu_order', 'asc')
            .then((result) => {
                resolve({
                    status: "ok",
                    data: {
                        data : result,
                        nama : decoded.data.nama,
                    }
                });
            }).catch((error) => {
            reject(error.message);
        });
    });
}

module.exports = {
    getMenu
};