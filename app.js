var express = require('express');
var app = express();

/**
 * 3 strategies can be used
 * single: Creates single database connection which is never closed.
 * pool: Creates pool of connections. Connection is auto release when response ends.
 * request: Creates new connection per new request. Connection is auto close when response ends.
 */

/**
 * setting up the templating views engine
 */
app.set('views engine', 'ejs');

var login = require('./routes/routes_login/index');
var home = require('./routes/routes_home/index');
var menu = require('./routes/routes_menu/index');
var user = require('./routes/routes_user/index');
var product = require('./routes/routes_product/index');
var image = require('./routes/routes_images');
var order = require('./routes/routes_order/index');
var chart = require('./routes/routes_chart/index');

/**
 * Express Validator Middleware for Form Validation
 */
app.use(express.static('public'));

/**
 * body-parser module is used to read HTTP POST data
 * it's an express middleware that reads form's input
 * and store it as javascript object
 */
var bodyParser = require('body-parser');
/**
 * bodyParser.urlencoded() parses the text as URL encoded data
 * (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body.
 */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var cookieParser = require('cookie-parser');

app.use(cookieParser('keyboard cat'));
app.use('/', login);
app.use('/home', home);
app.use('/menu', menu);
app.use('/user', user);
app.use('/product', product);
app.use('/image', image);
app.use('/order', order);
app.use('/chart', chart);

//CSS

app.listen(3000, function(){
    console.log('Server running at port 3001: http://127.0.0.1:3000')
});
module.exports = app;
