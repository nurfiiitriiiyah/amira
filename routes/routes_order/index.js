/**
 * Routing Untuk User
 * @author Nur FItriyah
 * @version 1.0
 * @since 02/13/2018
 */

/**
 * Module dependencies.
 */

const express = require('express');
const router = express.Router();
const formidableMiddleware = require('express-formidable');
const auth = require('../../backend/mod_helper/mod_auth');
const orders = require('../../backend/mod_order/index');

/**
 * Route untuk View Form User.
 * @name /product
 * @method GET
 */

router.get('/', function (req, res, next) {
    res.render('view_order/index.ejs', {title: 'aaaa'});
});

router.get('/product', function (req, res) {
    const token = req.headers['authorization'].replace('Bearer ', '');

    auth.authCheck(token).then((response) => {
        orders.getProduct(response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

/**
 * Route untuk Deactivate Produk
 * @name /product/deactivate
 * @method POST
 */
router.get('/detail', function (req, res) {
    let data = {
        orderProductType: req.query.idProduct,
        orderProdictDate: req.query.idTanggal
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        orders.getOrderSelect(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

/**
 * Route untuk Insert user
 * @name /insert/
 * @method POST
 */
router.post('/insert', formidableMiddleware(), function (req, res) {
    let data = {
        productTelephone: req.fields.add_telephone,
        productCostumer: req.fields.add_name,
        productID: req.fields.add_product,
        productDateRange: req.fields.add_tanggal,
        productPrice: req.fields.add_price,
        productPayment: req.fields.add_payment,
        productPaymentRemain: req.fields.add_payment_remain,
        productAddress: req.fields.add_address,
        productDestination: req.fields.add_destination
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        orders.InsertOrder(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

/**
 * Route untuk Mengambil List Data User untuk Datatable ServerSide.
 * @name /user/list
 * @method GEt
 */

router.get('/list', function (req, res) {
    let draw = req.query.draw;
    let start = parseInt(req.query.start);
    let length = parseInt(req.query.length);
    let order = req.query.order;

    console.log("****ROUTES****");
    let custom_search = {
        name: req.query.name,
        telephone: req.query.telephone,
        startDate:  req.query.startDate,
        endDate:  req.query.endDate,
        status: req.query.status,
        product: req.query.product
    };
    console.log(custom_search);

    const token = req.headers['authorization'].replace('Bearer ', '');

    auth.authCheck(token).then((response) => {
        orders.getOrder(start, length, draw, order, custom_search, response).then((response) => {
            res.json({
                draw: draw,
                recordsTotal: response.recordsTotal,
                recordsFiltered: response.recordsFiltered,
                data: response.data
            });
        });
    });
});

router.get('/detailOrder', function (req, res) {
    let id = req.query.idProduct;
    const token = req.headers['authorization'].replace('Bearer ', '');

    auth.authCheck(token).then((response) => {
        orders.getDetailOrder(id, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});


router.post('/reject', function (req, res) {
    let data = {
        order_id: req.body.order_id,
        notes: req.body.notes
    };
    const token = req.headers['authorization'].replace('Bearer ', '');

    auth.authCheck(token).then((responseAuth) => {
        orders.rejectOrder(data, responseAuth).then((responseSubmit) => {
            res.json({
                status: responseSubmit.status,
                data: responseSubmit.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({ auth: false, message: error });
    })
});

router.post('/lunas', function (req, res) {
    let data = {
        order_id: req.body.order_id,
        notes: req.body.notes
    };
    const token = req.headers['authorization'].replace('Bearer ', '');

    auth.authCheck(token).then((responseAuth) => {
        orders.PaymentDone(data, responseAuth).then((responseSubmit) => {
            res.json({
                status: responseSubmit.status,
                data: responseSubmit.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({ auth: false, message: error });
    })
});

module.exports = router;