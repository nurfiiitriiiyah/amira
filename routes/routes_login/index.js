const express = require('express');
const router = express.Router();
const login = require('../../backend/mod_login/index');
const formidableMiddleware = require('express-formidable');

router.get('/', function(req, res, next) {
    res.render('view_login/login.ejs', { title: 'aaaa' });
});

router.post('/', formidableMiddleware(), function(req, res, next) {
    var data = {
        sendUname : req.fields.username,
        sendPass : req.fields.password
    };
    login.login(data).then((response) => {
        res.json({
            status: response.status,
            data: response.data
        });
    }).catch((error) => {
        res.json({
            status: "nok",
            data: "Internal Server Error",
            errorMessage: error
        })
    });
});

module.exports = router;