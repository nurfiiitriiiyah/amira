/**
 * Routing Untuk Product
 * @author Nur FItriyah
 * @version 1.0
 * @since 24/11/2018
 */

/**
 * Module dependencies.
 */

const express = require('express');
const router = express.Router();
const fs = require(`fs`);
const http = require(`http`);

router.get('/', function (req, res, next) {
    let id = req.query.file;
    fs.readFile(id, function (err, data) {
        if (err) throw err;
        res.writeHead(200, {'Content-Type': 'image/jpeg'});
        res.end(data);
    });
});

module.exports = router;