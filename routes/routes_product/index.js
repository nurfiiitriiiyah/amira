/**
 * Routing Untuk Product
 * @author Nur FItriyah
 * @version 1.0
 * @since 24/11/2018
 */

/**
 * Module dependencies.
 */

const express = require('express');
const router = express.Router();
const formidableMiddleware = require('express-formidable');
const auth = require('../../backend/mod_helper/mod_auth');
const product = require('../../backend/mod_product/index');

/**
 * Route untuk View Form User.
 * @name /product
 * @method GET
 */

router.get('/', function (req, res, next) {
    res.render('view_product/index.ejs', {title: 'aaaa'});
});

/**
 * Route untuk Insert user
 * @name /insert/
 * @method POST
 */
router.post('/insert', formidableMiddleware(), function (req, res) {
    let data = {
        productName: req.fields.add_product_name,
        productSpesification: req.fields.add_product_spesification,
        productPhoto: req.files.add_product_photo
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        product.InsertProduct(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

/**
 * Route untuk Mengambil List Data User untuk Datatable ServerSide.
 * @name /user/list
 * @method GEt
 */

router.get('/list', function (req, res) {
    let draw = req.query.draw;
    let start = parseInt(req.query.start);
    let length = parseInt(req.query.length);
    let order = req.query.order;

    let custom_search = {
        name: req.query.name,
        status: req.query.status
    };
    const token = req.headers['authorization'].replace('Bearer ', '');

    auth.authCheck(token).then((response) => {
        product.getProduct(start, length, draw, order, custom_search, response).then((response) => {
            res.json({
                draw: draw,
                recordsTotal: response.recordsTotal,
                recordsFiltered: response.recordsFiltered,
                data: response.data
            });
        });
    });
});

router.get('/detail', function (req, res) {
    let id = req.query.idProduct;

    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        product.getDetailProduct(id, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

/**
 * Route untuk update user
 * @name /insert/
 * @method POST
 */
router.post('/edit', formidableMiddleware(), function (req, res) {
    let data = {
        productEditID: req.fields.edit_product_id,
        productEditName: req.fields.edit_product_name,
        prouctEditSpefication: req.fields.edit_product_spesification
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        product.EditProduct(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});


/**
 * Route untuk Activate Produk
 * @name /product/activate
 * @method POST
 */
router.post('/activate', formidableMiddleware(), function (req, res) {
    let data = {
        productActivateID: req.fields.activate_product_id
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        product.activateProduct(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

/**
 * Route untuk Deactivate Produk
 * @name /product/deactivate
 * @method POST
 */
router.post('/deactivate', formidableMiddleware(), function (req, res) {
    let data = {
        userDectivateUser: req.fields.activate_product_id
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        product.deactivateProduct(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

module.exports = router;