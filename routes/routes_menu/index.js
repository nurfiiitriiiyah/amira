/**
 * Routing Untuk Menu
 * @author Nur FItriyah
 * @version 1.0
 * @since 20/11/2018
 */

/**
 * Module dependencies.
 */

const express = require('express');
const router = express.Router();
const menu = require('../../backend/mod_menu/index');
const auth = require('../../backend/mod_helper/mod_auth');

/**
 * Route untuk Get List Menu.
 * @name /menu/
 * @method GET
 */

router.get('/', function (req, res, next) {
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        menu.getMenu(response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({ auth: false, message: error });
    });

});



module.exports = router;
