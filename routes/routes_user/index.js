/**
 * Routing Untuk User
 * @author Nur FItriyah
 * @version 1.0
 * @since 02/13/2018
 */

/**
 * Module dependencies.
 */

const express = require('express');
const router = express.Router();
const formidableMiddleware = require('express-formidable');
const auth = require('../../backend/mod_helper/mod_auth');
const user = require('../../backend/mod_user/index');

/**
 * Route untuk View Form User.
 * @name /product
 * @method GET
 */

router.get('/', function (req, res, next) {
    res.render('view_user/index.ejs', {title: 'aaaa'});
});

/**
 * Route untuk Insert user
 * @name /insert/
 * @method POST
 */
router.post('/insert', formidableMiddleware(), function (req, res) {
    let data = {
        userName: req.fields.add_name,
        userTelephone: req.fields.add_telephone,
        userEmail: req.fields.add_email,
        userRole: req.fields.add_role,
        userUname: req.fields.add_uname,
        userAddress: req.fields.add_address,
        userPass: req.fields.add_pass
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        user.InsertUser(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

/**
 * Route untuk Mengambil List Data User untuk Datatable ServerSide.
 * @name /user/list
 * @method GEt
 */

router.get('/list', function (req, res) {
    let draw = req.query.draw;
    let start = parseInt(req.query.start);
    let length = parseInt(req.query.length);
    let order = req.query.order;

    console.log("****ROUTES****");
    let custom_search = {
        name: req.query.name,
        telephone:  req.query.telephone,
        email:  req.query.email,
        address: req.query.address,
        roles: req.query.roles,
        uname: req.query.uname
    };
    console.log(custom_search);

    const token = req.headers['authorization'].replace('Bearer ', '');

    auth.authCheck(token).then((response) => {
        user.getUser(start, length, draw, order, custom_search, response).then((response) => {
            res.json({
                draw: draw,
                recordsTotal: response.recordsTotal,
                recordsFiltered: response.recordsFiltered,
                data: response.data
            });
        });
    });
});

router.get('/detail', function (req, res) {
    let id = req.query.idUser;

    const token = req.headers['authorization'].replace('Bearer ', '');

    auth.authCheck(token).then((response) => {
        user.getDetailUser(id, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});

/**
 * Route untuk update user
 * @name /insert/
 * @method POST
 */
router.post('/edit', formidableMiddleware(), function (req, res) {
    let data = {
        userEditID: req.fields.edit_id,
        userEditPhone: req.fields.edit_phone,
        userEditEmail: req.fields.edit_email,
        userEditAddress: req.fields.edit_address,
        userEditName: req.fields.edit_name
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        user.EditUser(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});


/**
 * Route untuk Activate User
 * @name /user/activate
 * @method POST
 */
router.post('/activate', function (req, res) {
    let data = {
        userActivateID: req.body.activate_id
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        user.activateUser(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});
/**
 * Route untuk Deactivate User
 * @name /user/deactivate
 * @method POST
 */
router.post('/deactivate', function (req, res) {
    let data = {
        userDectivateUser: req.body.activate_id
    };
    const token = req.headers['authorization'].replace('Bearer ', '');
    auth.authCheck(token).then((response) => {
        user.deactivateUser(data, response).then((response) => {
            res.json({
                status: response.status,
                data: response.data
            });
        }).catch((error) => {
            res.json({
                status: "nok",
                data: "Internal Server Error",
                errorMessage: error
            });
        });
    }).catch((error) => {
        return res.status(401).send({auth: false, message: error});
    });
});
module.exports = router;