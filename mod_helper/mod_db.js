const config = require('../config/main_config');
const mysql = require('mysql');
const connection = mysql.createPool({
    host     : config.prod.db_bost,
    user     : config.prod.db_uname,
    password : config.prod.db_pass,
    database : config.prod.db_name
});

module.exports = connection;
